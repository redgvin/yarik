# README #

This is a simple pseudo-game developed using MVC pattern

### Set up ###

Install: copy source to your hard drive.

Run: open index.html from root folder in any WebGL compatible web-browser

### What is it all about? (Game "rules") ###

* There is a rectangular area in the middle of your browser tab.
* The game starts with few random shapes with random colours on random positions.
* The shapes fall down from top to bottom (they are generated outside the top of the rectangle, and fall through the bottom of the
rectangle). The falling is controlled by the Gravity Value.
* If you click inside the rectangle, a random shape of random colour will be
generated at mouse position and start falling from this position.
* If you click at any shape, it will disappear.
* In the top you have 2 indicators, one shows how much shapes are on screen now, another -
shows the area of these shapes in square pixels 

### How does this game look like? ###
See screenshots below:

![picture](img/example.png)
and
![picture](img/example2.png)

