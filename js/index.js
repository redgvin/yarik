import { gameModel } from './models/gamemodel.js';
import { gameController } from './controllers/gamecontroller.js';
import { gameView } from './views/gameview.js';

console.clear();

//initialize and start a game on document load
window.addEventListener('load', () => {
  const model = new gameModel();
  const view = new gameView();
  const controller = new gameController(model, view);
  controller.initializeGame();
});

