import {EventEmitter} from "../eventdispatchers/eventdispatcher.js";

/**
 * The View. View presents the model and provides
 * the UI events. The controller is attached to these
 * events to handle the user interaction.
 */
export class gameView extends EventEmitter {
    constructor() {
        super();

        this.elements = {
            'canvas' : document.getElementById('mvc_canvas'),
            'stage_sprite': new PIXI.Sprite(),
            'shapesQtyIndicator' : document.getElementById('shapesQty'),
            'surfaceAreaIndicator' : document.getElementById('shapesArea'),
            'increaseGravityBtn' : document.getElementById('gravityPlusBtn'),
            'decreaseGravityBtn' : document.getElementById('gravityMinusBtn'),
            'increaseShapesQtyBtn' : document.getElementById('plusBtn'),
            'decreaseShapesQtyBtn' : document.getElementById('minusBtn'),
            'gravityModifierInput' : document.getElementById('gravityModifier'),
            'shapesQtyModifierInput' : document.getElementById('shapesQtyModifier')

        };
        // attach listeners to HTML controls
        this.elements.increaseShapesQtyBtn.addEventListener('click',
            () => this.emit('increaseShapesQtyBtnClicked'));
        this.elements.decreaseShapesQtyBtn.addEventListener('click',
            () => this.emit('decreaseShapesQtyBtnClicked'));

        this.elements.increaseGravityBtn.addEventListener('click',
            () => this.emit('increaseGravityBtnClicked'));
        this.elements.decreaseGravityBtn.addEventListener('click',
            () => this.emit('decreaseGravityBtnClicked'));
        this.elements.gravityModifierInput.addEventListener('input',
            (newGravityValue) => {this.emit('gravityModifierInput', newGravityValue)});
        this.elements.shapesQtyModifierInput.addEventListener('input',
            (newQtyValue) => {this.emit('shapesQtyModifierInput',newQtyValue)});

    }

}
