//functions for calculation and useful generators
//make random coordinates for a polygon
export function generateCoords(noOfPoints){
    let x = [0];
    let y = [0];
    let coords = []; //array to return
    let r = 0;
    let angle = 0;
    for (let i = 1; i < noOfPoints; i++) {
        angle += 0.3 + Math.random() * 0.4
        if (angle > 2 * Math.PI) {
            break; //stop before it becomes convex
        }
        r = (2 + Math.random() * 20+Math.random()*25);
        let valX = x[i - 1] + Math.floor(r * Math.cos(angle));
        x.push(valX);
        let valY = y[i - 1] + Math.floor(r * Math.sin(angle));
        y.push(valY);
        coords.push({x: valX, y: valY});
    }
    return coords;
}

export function getRandomHexColour() {
    return '0x'+Math.floor(Math.random()*16777215).toString(16);
}

//random side quantity block
export function getRandomSidesQuantity() {
    var qty = Math.floor(Math.random() * (9));
    if (qty<3){
        return 0; //we can't have 2-sided or 1-sided shape duh
    } else return qty
}

