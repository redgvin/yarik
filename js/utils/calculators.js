//functions for calculation and useful generators
//polygon area calculation
export function shoelaceArea(coordsList) {
    coordsList.pop(coordsList.length-1);
    let n = coordsList.length;
    let a = 0.0;
    for (let i = 0; i<(n - 1); i++) {
        a += coordsList[i].x * coordsList[i + 1].y - coordsList[i + 1].x * coordsList[i].y
    }
    return Math.floor(Math.abs(a + coordsList[n - 1].x * coordsList[0].y - coordsList[0].x * coordsList[n -1].y) / 2.0)
}