/**
 * The Controller. Controller responds to user actions and
 * invokes changes on the model.
 */
export class gameController {
    constructor(gameModel, gameView) {
        this._model = gameModel;
        this._view = gameView;

        //attach model listeners
        gameModel.on('canvasClick', (e) => this.processCanvasClick(e));
        gameModel.on('modifiedShapesQtyModifier', (shapes_qty) => {this._view.elements.shapesQtyModifierInput.value = shapes_qty });
        gameModel.on('modifiedGravityModifier', (gravity) => {this._view.elements.gravityModifierInput.value = gravity });
        gameModel.on('shapesQtyIndicatiorRefresh', (e) => {this._view.elements.shapesQtyIndicator.value = e});
        gameModel.on('totalShapesAreaRefresh', (e) => {this._view.elements.surfaceAreaIndicator.value = e});

        gameView.on('increaseShapesQtyBtnClicked', () => this.increaseShapesQtyBtnClicked());
        gameView.on('decreaseShapesQtyBtnClicked', () => this.decreaseShapesQtyBtnClicked());
        gameView.on('increaseGravityBtnClicked', () => this.increaseGravityBtnClicked());
        gameView.on('decreaseGravityBtnClicked', () => this.decreaseGravityBtnClicked());
        gameView.on('gravityModifierInput', (e) => this.modifyGravity(e));
        gameView.on('shapesQtyModifierInput', (e) => this.modifyShapesGenerationQuantity(e));
    }

    processCanvasClick(e) {
        //blink a border? for later development

    }

    modifyGravity(e) {
        this._model.setGravity(parseInt(e.target.value));
    }

    modifyShapesGenerationQuantity(e) {
        this._model.setShapesGenerateVelocity(parseInt(e.target.value));
    }

    increaseShapesQtyBtnClicked(){
        this._model.increaseShapesGenerateVelocity()
    }

    decreaseShapesQtyBtnClicked(){
        this._model.decreaseShapesGenerateVelocity()
    }

    increaseGravityBtnClicked(){
        this._model.increaseGravity()
    }

    decreaseGravityBtnClicked(){
        this._model.decreaseGravity()
    }

    initializeGame() {
        this._model.initializeCanvas();
        this.startGame();
    }

    // Listen for animate update
    startGame(){
        this._model.app.ticker.add((delta) => {
            for (let i = 0; i<this._model.shapesStack.length; i++){
                this._model.shapesStack[i].moveShape(this._model.gravity);
            }
            //clean shapesStack
            let dirtyShapesStack = this._model.shapesStack;
            this._model.shapesStack = [];
            for (let i = 0; i<dirtyShapesStack.length; i++){
                if (dirtyShapesStack[i].deletionMark === 0 && dirtyShapesStack[i].coords[1]<this._model.lowerBorder){
                    this._model.shapesStack.push(dirtyShapesStack[i]);
                }
            }
            this._model.calculateTotalArea();
            const secondsNow = new Date().getSeconds();
            if (secondsNow !== this._model.currentSecond) {
                let numberGenerated = 0;
                for (let n=1; n<=this._model.shapesGenerationVelocity; n++){
                    let randomX = 40+Math.floor(Math.random() * (520 + 1)) ;
                    let randomY = -500+Math.floor(Math.random() * (450 + 1));
                    this._model.generateShapeOnCoordinates(randomX, randomY);
                    numberGenerated++;
                }
                this._model.currentSecond = secondsNow;
            }
            this._model.currentSecond = secondsNow;
            this._model.emit('shapesQtyIndicatiorRefresh', this._model.shapesStack.length);
        });
    };
}
